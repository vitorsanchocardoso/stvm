﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace STVM {
    // Classe contém estrutura da resposta aninhada no atributo "Results", que, por sua vez,
    // está implementado em "STVMResponse.cs"
    public class STVM {
        [JsonProperty("flowType")] // FLUXO
        public string? FlowType { get; set; }

        [JsonProperty("transferType")] // tipo transferencia
        public string? TransferType { get; set; }

        [JsonProperty("fundSubAccount")] // pco
        public string? FundSubAccount { get; set; }

        [JsonProperty("customerName")] // nome cliente
        public string? CustomerName { get; set; }

        [JsonProperty("fundManagerName")] // fundo
        public string? FundManagerName { get; set; }

        [JsonProperty("processStatus")] // Status do Processo
        public ProcessStatusAPI? ProcessStatus { get; set; }

        [JsonProperty("documents")]
        public List<Documento> Documents { get; set; }

        [JsonProperty("fundCNPJ")] // CNPJ FUNDO
        public string? FundCNPJ { get; set; }

        [JsonProperty("issuerName")] //  NOME CONTRAPARTE
        public string? IssuerName { get; set; }

        [JsonProperty("fundTemaId")] // COD FUNDO PRESTADORA
        public string? FundTemaId { get; set; }

        [JsonProperty("fundAdministrator")] // ADM FUNDO
        public string? FundAdministrator { get; set; }

        [JsonProperty("createdAt")]  // DATA CADASTRO
        public System.DateTime CreatedAt { get; set; }

        public override string ToString() {

            // Tratamento de dados (campo FlowType)
            if (FlowType == "input") {
                FlowType = "Entrada";
            } else {
                FlowType = "Saída";
            }

            return string.Join(",",
                FundSubAccount.Substring(5,8), // PCO
                FundCNPJ, // CNPJ_FUNDO
                IssuerName, // CONTRAPARTE
                "", // BLOCO (NAO RETORNA PELA API)
                FlowType, // FLUXO (ENTRADA OU SAIDA)
                TransferType, // Tipo (PCO X PCO, rc)
                CreatedAt, // DT_RECEBIMENTO
                "", // DT_CONCLUSAO (NAO RETORNA PELA API)
                "", // COD_PCO_TRANSF
                "", // COD_ADM_ORAMA
                "", // COD_ADM_TRANSF
                "", // ENDERECO (NAO RETORNA PELA API)
                "01 - Comercial cadastrou", // STATUS_TRANSF
                "", // DT_ATUALIZACAO (NAO RETORNA PELA API)
                "", // USUARIO (NAO RETORNA PELA API)
                "", // OBSERVACOES (NAO RETORNA PELA API)
                string.Join("#", Documents.Select(x => x.DocumentName)), // Nome do pdf anexado
                string.Join("#", Documents.Select(x => x.Presigned_url)) // link para download do pdf_anexado
                );
        }
    }
}

