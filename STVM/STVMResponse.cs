﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace STVM
{
    public class STVMResponse
    {
        [JsonProperty("results")]
        public List<STVM>? Results { get; set; }
    }
}
