﻿using Refit;
using System.Threading.Tasks;

namespace STVM
{
    [Headers("Authorization: token 893466beab584a49aa7c8c1875bea157")]
    public interface ISTVMAPIService
    {
        [Get("/v1/fund-position-migrations-macro/")]
        Task<STVMResponse> GetSTVMs();
    }
}
