﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STVM {
    public class Documento {

        [JsonProperty("documentName")]
        public string? DocumentName { get; set; }

        [JsonProperty("presigned_url")]
        public string? Presigned_url { get; set; }
    }
}
