﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STVM {
    public class ProcessStatusAPI {

        [JsonProperty("description")] // Descricao Status
        public string? Description { get; set; }
    }
}
