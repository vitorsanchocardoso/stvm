﻿using Refit;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace STVM
{
    public class Progrma
    {
        public static async Task Main(string[] args)
        {
            try
            {
		// testando git
                // nome arquivo txt
                //string txtFile = @"C:\Users\felipe.souza\Downloads\stvmApiSwagger.txt";
                string txtFile = args[0];

                // Cria arquivo txt para posterior inclusao de dados
                StreamWriter texto = File.CreateText(txtFile);

                var stvmClient = RestService.For<ISTVMAPIService>("https://pre-fundos.orama.dev");

                var stvmResponse = await stvmClient.GetSTVMs();

                // Escreve cabecalho no txt
                texto.WriteLine("COD_PCO_ORAMA,CNPJ_FUNDO,CONTRAPARTE," +
                    "BLOCO,FLUXO,TIPO,DT_RECEBIMENTO,DT_CONCLUSAO," +
                    "COD_PCO_TRANSF,COD_ADM_ORAMA,COD_ADM_TRANSF," +
                    "ENDERECO,STATUS_TRANSF,DT_ATUALIZACAO,USUARIO,OBSERVACOES,NOME_ARQUIVO,LINK_ARQUIVO");

                foreach (var stvm in stvmResponse.Results)
                {
                    string statDescription = stvm.ProcessStatus.Description;
                    // diferenca de dias entre hoje e a data de solicitacao da STVM no Fogao (API)
                    var daysDiff = (DateTime.Now - stvm.CreatedAt).Days;
                    // verificar se consigo filtrar a data na propria query da api
                    if (daysDiff <= 5 && statDescription != "Cancelado") {
                        var linhaTXT = stvm.ToString();
                        // escreve a linha no txt
                        texto.WriteLine(linhaTXT, Encoding.GetEncoding("ISO-8859-1"));
                    }
                }
                texto.Close(); // Fecha o arquivo salvando-o
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}